import createServer from './server';
import RhinocerosModel from './rhinoceros';

const PORT = process.env.PORT || 3000;
const model = new RhinocerosModel();
const server = createServer(model);

// eslint-disable-next-line no-console
console.log(`Server listening on port: ${PORT}`);
server.listen(PORT);
