import Router from 'koa-router';
import { ValidationError } from './errors';
import RhinocerosModel from './rhinoceros';

export default function (model: RhinocerosModel): Router {
  const router = new Router();

  router.get('/rhinoceros', (ctx) => {
    const rhinoceroses = model.getAll(ctx.query);
    ctx.response.body = { rhinoceroses };
  });

  router.get('/endangered-rhinoceros', (ctx) => {
    const rhinoceroses = model.getEndangered();
    ctx.response.body = { rhinoceroses };
  });

  /**
   * finds a rhino by ID.
   * if no rhino with the given ID is found, a 404 response is sent
   */
  router.get('/rhinoceros/:id', (ctx) => {
    const rhinoceros = model.findById(ctx.params.id);

    if (rhinoceros === undefined) {
      ctx.throw(404);
    } else {
      ctx.response.body = rhinoceros;
    }
  });

  router.post('/rhinoceros', (ctx) => {
    try {
      const rhino = model.newRhinoceros(ctx.request.body);
      ctx.response.body = rhino;
    } catch (error) {
      if (error instanceof ValidationError) {
        ctx.response.body = error.errors;
        ctx.response.status = 400;
      } else {
        throw error;
      }
    }
  });

  return router;
}
