import { ErrorObject } from 'ajv';

export class ValidationError extends Error {
  public errors: ErrorObject[];

  constructor(errors: ErrorObject[]) {
    super('Validation Error');
    this.errors = errors;
  }
}
