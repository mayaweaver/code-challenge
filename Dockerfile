FROM node:12.16.1

WORKDIR /rhino

ADD ./package.json .
ADD ./package-lock.json .
RUN npm install

ADD ./src ./src
ADD ./tsconfig.json .
RUN npm run build

ENV NODE_PATH /rhino

CMD node ./dist/index.js
