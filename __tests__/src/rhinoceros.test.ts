import RhinocerosModel from '../../src/rhinoceros';
import { ValidationError } from '../../src/errors';
import mockRhino from '../../__mocks__/rhino';

describe('rhinoceros', () => {
  describe('getAll', () => {
    describe('by default', () => {
      it('returns all the rhinos from the store', () => {
        const store = [
          mockRhino(),
          mockRhino(),
          mockRhino(),
          mockRhino(),
        ];
        const model = new RhinocerosModel(store);

        expect(model.getAll()).toStrictEqual(store);
      });
    });

    describe('when given a name', () => {
      it('only returns rhinos with the given name', () => {
        const store = [
          mockRhino(),
          mockRhino(),
        ];

        const model = new RhinocerosModel(store);

        store.forEach((rhino) => {
          const rhinos = model.getAll({
            name: rhino.name,
          });

          expect(rhinos).toStrictEqual([rhino]);
        });
      });
    });

    describe('when given a species', () => {
      it('only returns rhinos with the given species', () => {
        const store = [
          { ...mockRhino(), species: 'white_rhinoceros' },
          { ...mockRhino(), species: 'indian_rhinoceros' },
          { ...mockRhino(), species: 'javan_rhinoceros' },
          { ...mockRhino(), species: 'javan_rhinoceros' },
          { ...mockRhino(), species: 'black_rhinoceros' },
        ];

        const model = new RhinocerosModel(store);

        const whiteRhinos = model.getAll({ species: 'white_rhinoceros' });
        expect(whiteRhinos).toHaveLength(1);
        expect(
          whiteRhinos.every((rhino) => rhino.species === 'white_rhinoceros'),
        ).toBeTruthy();

        const indianRhinos = model.getAll({ species: 'indian_rhinoceros' });
        expect(indianRhinos).toHaveLength(1);
        expect(
          indianRhinos.every((rhino) => rhino.species === 'indian_rhinoceros'),
        ).toBeTruthy();

        const javanRhinos = model.getAll({ species: 'javan_rhinoceros' });
        expect(javanRhinos).toHaveLength(2);
        expect(
          javanRhinos.every((rhino) => rhino.species === 'javan_rhinoceros'),
        ).toBeTruthy();

        const blackRhinos = model.getAll({ species: 'black_rhinoceros' });
        expect(blackRhinos).toHaveLength(1);
        expect(
          blackRhinos.every((rhino) => rhino.species === 'black_rhinoceros'),
        ).toBeTruthy();
      });
    });

    describe('when given a species and a name', () => {
      it('only returns rhinos with the given species and name', () => {
        const store = [
          { ...mockRhino(), name: 'alice', species: 'white_rhinoceros' },
          { ...mockRhino(), name: 'bob', species: 'indian_rhinoceros' },
          { ...mockRhino(), name: 'charlie', species: 'javan_rhinoceros' },
          { ...mockRhino(), name: 'alice', species: 'javan_rhinoceros' },
          { ...mockRhino(), name: 'bob', species: 'black_rhinoceros' },
        ];

        const model = new RhinocerosModel(store);

        expect(
          model.getAll({
            name: 'alice',
            species: 'white_rhinoceros',
          }),
        ).toStrictEqual([
          store[0],
        ]);

        expect(
          model.getAll({
            name: 'bob',
            species: 'indian_rhinoceros',
          }),
        ).toStrictEqual([
          store[1],
        ]);

        expect(
          model.getAll({
            name: 'charlie',
            species: 'black_rhinoceros',
          }),
        ).toStrictEqual([]);
      });
    });
  });

  describe('getEndangered', () => {
    it('returns an empty array when the store has no rhinos', () => {
      const store = [];
      const model = new RhinocerosModel(store);

      expect(model.getEndangered()).toStrictEqual([]);
    });

    it('returns an array with the only rhino when the store has only one rhino', () => {
      const rhino = mockRhino();
      const store = [rhino];
      const model = new RhinocerosModel(store);

      expect(model.getEndangered()).toStrictEqual([rhino]);
    });

    it('returns an array with the endangered rhino when the store has endangered and non-endangered rhinos', () => {
      const store = [
        { ...mockRhino(), species: 'black_rhinoceros' },
        { ...mockRhino(), species: 'white_rhinoceros' },
        { ...mockRhino(), species: 'black_rhinoceros' },
        { ...mockRhino(), species: 'black_rhinoceros' },
      ];
      const model = new RhinocerosModel(store);

      expect(model.getEndangered()).toStrictEqual([store[1]]);
    });

    it('returns an empty array when the store has no endangered rhinos', () => {
      const store = [
        { ...mockRhino(), species: 'black_rhinoceros' },
        { ...mockRhino(), species: 'black_rhinoceros' },
        { ...mockRhino(), species: 'black_rhinoceros' },
        { ...mockRhino(), species: 'white_rhinoceros' },
        { ...mockRhino(), species: 'white_rhinoceros' },
        { ...mockRhino(), species: 'white_rhinoceros' },
      ];
      const model = new RhinocerosModel(store);

      expect(model.getEndangered()).toStrictEqual([]);
    });
  });

  describe('newRhinoceros', () => {
    const invalidCases = {
      name: [
        undefined,
        '',
        'a'.repeat(21),
      ],
      species: [
        undefined,
        '',
        'example-unknown-species',
      ],
    };

    invalidCases.name.forEach((name) => {
      invalidCases.species.forEach((species) => {
        it(`throws a ValidationError when given name: ${name}, species: ${species}`, () => {
          const model = new RhinocerosModel();

          expect(() => {
            model.newRhinoceros({
              name,
              species,
            });
          }).toThrow(ValidationError);
        });
      });
    });

    describe('when given valid data', () => {
      it('does not throw a ValidationError', () => {
        expect(() => {
          const model = new RhinocerosModel();
          const rhino = mockRhino();

          expect(() => {
            model.newRhinoceros({
              name: rhino.name,
              species: rhino.species,
            });
          }).not.toThrow(ValidationError);
        });
      });

      it('returns the given name', () => {
        expect(() => {
          const model = new RhinocerosModel();
          const rhino = mockRhino();

          const newRhino = model.newRhinoceros({
            name: rhino.name,
            species: rhino.species,
          });

          expect(newRhino.name).toStrictEqual(rhino.name);
        });
      });

      it('returns the given species', () => {
        expect(() => {
          const model = new RhinocerosModel();
          const rhino = mockRhino();

          const newRhino = model.newRhinoceros({
            name: rhino.name,
            species: rhino.species,
          });

          expect(newRhino.species).toStrictEqual(rhino.species);
        });
      });
    });
  });

  describe('findById', () => {
    it('returns undefined when the store is empty', () => {
      const store = [];
      const model = new RhinocerosModel(store);
      const { id } = mockRhino();

      expect(model.findById(id)).toBeUndefined();
    });

    it('returns undefined when given an unknown ID', () => {
      const store = [mockRhino()];
      const model = new RhinocerosModel(store);
      const { id } = mockRhino();

      expect(model.findById(id)).toBeUndefined();
    });

    it('returns the correct rhino when given a known ID', () => {
      const rhino = mockRhino();
      const store = [rhino];
      const model = new RhinocerosModel(store);

      expect(model.findById(rhino.id)).toStrictEqual(rhino);
    });

    it('returns the correct rhinos when selecting from a store with multiple rhinos', () => {
      const rhinoA = mockRhino();
      const rhinoB = mockRhino();
      const store = [rhinoA, rhinoB];
      const model = new RhinocerosModel(store);

      expect(model.findById(rhinoA.id)).toStrictEqual(rhinoA);
      expect(model.findById(rhinoB.id)).toStrictEqual(rhinoB);
    });

    it('returns the correct rhino that\'s been added via newRhinoceros()', () => {
      const store = [];
      const model = new RhinocerosModel(store);
      const rhino = model.newRhinoceros({
        name: mockRhino().name,
        species: mockRhino().species,
      });

      expect(model.findById(rhino.id)).toStrictEqual(rhino);
    });
  });
});
