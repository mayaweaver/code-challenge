import faker from 'faker';
import request from 'supertest';
import AJV from 'ajv';
import createServer from '../../src/server';
import RhinocerosModel, { Species } from '../../src/rhinoceros';
import mockRhino from '../../__mocks__/rhino';

const ajv = new AJV();

interface SchemaSet {
  [key: string]: object;
}

const schemas = {} as SchemaSet;
schemas.rhino = {
  type: 'object',
  properties: {
    id: {
      type: 'string',
    },
    name: {
      type: 'string',
      minLength: 1,
      maxLength: 20,
    },
    species: {
      type: 'string',
      enum: Species,
    },
  },
  required: [
    'id',
    'name',
    'species',
  ],
  additionalProperties: false,
};

function matchSchema(obj: object, schema: object): void {
  const validator = ajv.compile(schema);

  const valid = validator(obj);

  if (!valid) {
    console.log(validator.errors);
  }

  expect(valid).toBeTruthy();
}

describe('createServer', () => {
  it('does not throw an error', () => {
    expect(() => {
      const model = new RhinocerosModel();
      createServer(model);
    }).not.toThrow();
  });

  describe('GET /rhinoceros', () => {
    it('returns a list of rhinos', async () => {
      const model = new RhinocerosModel();
      const server = createServer(model);

      const response = await request(server.callback())
        .get('/rhinoceros');

      expect(response.status).toBe(200);
      matchSchema(response.body, {
        type: 'object',
        properties: {
          rhinoceroses: {
            type: 'array',
            items: schemas.rhino,
          },
        },
        required: [
          'rhinoceroses',
        ],
      });
    });
  });

  describe('GET /endangered-rhinoceros', () => {
    it('returns a list of rhinos', async () => {
      const model = new RhinocerosModel();
      const server = createServer(model);

      const response = await request(server.callback())
        .get('/endangered-rhinoceros');

      expect(response.status).toBe(200);
      matchSchema(response.body, {
        type: 'object',
        properties: {
          rhinoceroses: {
            type: 'array',
            items: schemas.rhino,
          },
        },
        required: [
          'rhinoceroses',
        ],
      });
    });
  });

  describe('GET /rhinoceros/:id', () => {
    it('returns a list of rhinos', async () => {
      const rhino = mockRhino();
      const model = new RhinocerosModel([rhino]);
      const server = createServer(model);

      const response = await request(server.callback())
        .get(`/rhinoceros/${rhino.id}`);

      expect(response.status).toBe(200);
      matchSchema(response.body, schemas.rhino);
    });
  });

  describe('POST /rhinoceros', () => {
    it('returns a list of rhinos', async () => {
      const rhino = {
        name: mockRhino().name,
        species: mockRhino().species,
      };
      const model = new RhinocerosModel();
      const server = createServer(model);

      const response = await request(server.callback())
        .post('/rhinoceros')
        .send(rhino);

      if (response.status !== 200) {
        console.log(response.body);
      }

      expect(response.status).toBe(200);
      matchSchema(response.body, schemas.rhino);
    });
  });
});
